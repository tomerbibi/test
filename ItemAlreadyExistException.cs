﻿using System;
using System.Runtime.Serialization;

namespace TestPart2Generics
{
    [Serializable]
    internal class ItemAlreadyExistException : Exception
    {
        public ItemAlreadyExistException()
        {
        }

        public ItemAlreadyExistException(string message) : base(message)
        {
        }

        public ItemAlreadyExistException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected ItemAlreadyExistException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}