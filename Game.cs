﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestPart3_1_
{
    class Game
    {
        List<string> _words;
        Random r = new Random();

        public Game(List<string> words)
        {
            _words = words;
        }
        private int guessAmount { get; set; }
        public void start()
        {
            guessAmount = 0;
            int index = r.Next(0, _words.Count - 1);
            string realWord = _words[index];
            List<string> gameWord = new List<string>();
            gameWord.AddRange(realWord.Select(c => c.ToString()));
            List<string> userWord = new List<string>();
            for (int i = 0; i < realWord.Length; i++)
            {
                userWord.Add("_");
            }
            while (userWord.Contains("_"))
            {
                guessAmount++;
                for (int i = 0; i < userWord.Count; i++)
                {
                    Console.Write(userWord[i]);
                }
                string userLetter = Console.ReadLine();
                if (realWord.Contains(userLetter))
                {
                    for (int i = 0; i < gameWord.Count; i++)
                    {
                        if (gameWord[i] == userLetter)
                            userWord[i] = gameWord[i];
                    }
                }
            }
            Console.WriteLine($"you won after {guessAmount} guesses. do you want to play again?");
            if (Console.ReadLine() == "yes")
                start();
        }

    }
}
