﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestPart3_2_
{
    class Rational
    {
        public int Mone { get; set; }
        public double Mehane { get; set; }
        public double Num { get; set; } // i just created the property Num which make is shorter for the functions 
        public Rational(int mone, double mehane)
        {
            Mone = mone;
            Mehane = mehane;
            if (mehane <= 0)
                Num = 0;
            else
                Num = (double)mone / mehane;
        }
        public bool GreaterThan(double d)
        {
 /*           theres no reason to check if the number that we get is a rational number because if it isnt the return value from the function
            will be the same anyway and same goes for the other functions*/
            return Num > d;
        }
        public static double operator +(Rational that, Rational other)
        {
            return that.Num + other.Num;
        }
        public static double operator -(Rational that, Rational other)
        {
            return that.Num - other.Num;
        }
        public static double operator *(Rational that, Rational other)
        {
            return that.Num * other.Num;
        }
        public int GetNumerator()
        {
            return Mone;
        }
        public double GetDenominator()
        {
            return Mehane;
        }
        public bool Equals(Rational r)
        {
            return Num == r.Num;
        }

        public override string ToString()
        {
            return $"{Mone}/{Mehane}";
        }
    }
}
